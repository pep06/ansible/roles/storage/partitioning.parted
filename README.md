# Ansible role ``storage/partitioning.parted``

## Description

Configure storage device partitioning with parted.
